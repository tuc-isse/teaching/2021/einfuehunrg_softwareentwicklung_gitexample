package tuc.buildingExample;

/**
 * Ein Raum auf einer Etage des Gebäudes. Der Raum verfügt über eine bestimmte Anzahl Fester.
 * @author Sebastian
 */
public class Room {
	private Window[] windows;
	private int number;
	private Elevator elevator;
	
	/**
	 * Initiiert den Raum und seine Fenster.
	 * @param roomNumber Raumnummer. Diese besteht immer aus einer hunderter Stelle, für die Etage + die Nummer des Raumes auf der Etage.
	 * z.B. der 3.  Raum auf der 4. Etage ist 403,
	 * 		der 10. Raum auf der 9. Etage ist 910
	 * 
	 * @param windowCount Anzahl der Fenster.
	 * @param elevator Eine Referenz auf den Aufzug, oder null, falls der Raum keinen Zugang zum Aufzug besitzt.
	 */
	public Room(int roomNumber, int windowCount, Elevator elevator) {
		// Implementieren Sie den Konstruktor.
	}

	/**
	 * @return Gibt zurück, ob der Raum einen Zugang zum Aufzug besitzt.
	 */
	public boolean hasElevator() {
		return elevator !=null;
	}
}
