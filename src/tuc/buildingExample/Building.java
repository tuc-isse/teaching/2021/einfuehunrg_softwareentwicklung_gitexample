package tuc.buildingExample;

/**
 * Diese Klasse Beschreibt ein Gebäude... 
 * @author Sebastian 
 *eine �nderung
 */
public class Building {
	private Floor[] floors;
	private Elevator elevator;
	
	/**
	 * Hier wird das Gebäude erstellt und seine Etagen angelegt. 
	 * Der Zugang zum Aufzug soll immer bei dem letzen Raum einer Etage erfolgen.
	 * @param numberOfFloors Anzahl der Etagen.
	 * @param roomsPerFloor Anzahl der Raume in jeder Etage
	 */
	public Building() {
		}
	
	
	/**
	 * @param floorNumber Position der gesuchten Etage im Array.
	 * @return Etage mit der Angegebenen Nummer;
	 */
	public Floor getFloor(int floorNumber) {
		return floors[floorNumber];
	}
	
	/**	
	 * @return Anzahl der Etagen.
	 */
	public int getFloorCount() {
		return this.floors.length;
	}
}
